## Providers

| Name | Version |
|------|---------|
| google | n/a |
| google-beta | n/a |

## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:-----:|
| name | Environment name | `string` | n/a | yes |
| node\_pool\_autoscaling\_max\_node\_count | Maximum nodes in cluster = value \* availability zones count | `number` | `1` | no |
| node\_pool\_autoscaling\_min\_node\_count | Minimum nodes in cluster = value \* availability zones count | `number` | `1` | no |
| project | GCP project ID | `string` | n/a | yes |
| region | GCP region | `string` | n/a | yes |
| worker\_disk\_size | Boot disk size (GB) for worker nodes | `number` | `20` | no |
| worker\_machine\_type | Worker node's machine type | `string` | `"n1-standard-2"` | no |

## Outputs

| Name | Description |
|------|-------------|
| cluster\_endpoint | Public endpoint of Kubernetes cluster |
| service\_external\_ip\_address | External IP address for ingress |

